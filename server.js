console.log("Hola Mundo")
var express = require('express');
var app = express();
var port = process.env.PORT|| 3000;

//Para rellenar el Body vacío//
var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);

console.log("API Molona escuchando en el puerto" + port);


app.get ("/apitechu/v1",
  function(req,res) {
    // Aconsejabe poner en el log, el método y la ruta pàra saber que ejecuta//
    console.log("GET /apitechu/v1");
    res.send({"msg": "Respuesta desde  Pechu"});

  }
);


//Sesión 24/04/2018//

app.get("/apitechu/v1/users",
  function(req, res){
    console.log("GET /apitechu/v1/users");

    //res.sendFile('usuarios.json', )//
    //__dirname es el directorio acual donde se ejecuta el json//
    // el resultado se deja en el sendFile //

    res.sendFile('usuarios.json', {root: __dirname});
    // se puede obtener el fichero directamente haciendo un require//

    //var users  = require('./usuarios.json');//
    //res.send(users);//
  }
);


//SE CREA UN USUARIO// es importante tener estas trazas al inicio de cada ruta//
app.post("/apitechu/v1/login",
function(req, res){
  console.log("POST /apitechu/v1/login");
  console.log("email is: " + req.body.email);
  console.log("pass is: " +  req.body.password);

  var newUser = {
    "email": req.body.email,
    "pass" : req.body.password
  };

  var_msg = {
    "mensaje":"Login Incorrecto"
      }

  var users  = require('./usuarios.json'); //es una array////se reogen los emails y passwords fichero.json en users//

  for (user of users) {
    if ((newUser.email == user.email) && (newUser.pass == user.password)){

    console.log("Login Correcto");

    var_msg = {
      "mensaje":"Login Correcto",
      "idUsuario":user.id
    }


    user.logged = true;
    writeUserDataToFile(users);
    break;
    }

    else {
      console.log("Login Incorrecto");

    }

  }

  console.log("Fin");
  res.send(var_msg);
  }
);


app.post("/apitechu/v1/logout",
function(req, res){
  console.log("POST /apitechu/v1/logout");
  console.log("id is: " + req.body.id);

  var borraUser = {
    "id": req.body.id
      };

  var_msg = {
    "mensaje":"Logout Incorrecto"
      }

  var users  = require('./usuarios.json'); //es una array////se reogen los emails y passwords fichero.json en users//

  for (user of users) {
    if (borraUser.id == user.id){

    delete user.logged;
    console.log("Logout Correcto");
    writeUserDataToFile(users);

    var_msg = {
      "mensaje":"Logout Correcto",
      "id" : req.body.id
    }




    break;
    }

    else {
      console.log("Logout Incorrecto");

    }

  }

  console.log("Fin");
  res.send(var_msg);
  }
);






//Borrado de un Registro//
app.delete("/apitechu/v1/users/:id",
function(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log(req.params);
  console.log(req.params.id);

  var users  = require('./usuarios.json');
  users.splice(req.params.id - 1, 1) ;
  writeUserDataToFile(users);
  console.log("Usuario Borrado");
  res.send({"msg":"Usuario borrado"});
}
);

//funtion writeUserDataToFile(data)//

function writeUserDataToFile(data){
//libreria fs//
	var fs = require('fs');
	//con stringify convierte un js en un json//
	var jsonUserData = JSON.stringify(data);

	fs.writeFile("./usuarios.json", jsonUserData, "utf8",
	//fichero que se va a escribir, lo que se va a escribir, tipo de almacenaje//
	 function(err) {
	   if (err) {
		 console.log(err);
	   }
	   else {
		 console.log("ok al escribir fichero de usuarios");
	   }
	}
	)
}



//Sesión 25/04/2018//

//SE prueban los métodos //
app.post("/apitechu/v1/monstruo/:p1/:p2",
function(req, res){
  console.log("Parametros");
  console.log(req.params);

  console.log("Query String");
  console.log(req.query);

  console.log("Body");
  console.log(req.body);

  console.log("Cabeceras");
  console.log(req.headers);
}
)
